//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "ACE3P.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/common/UUID.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"
#include "smtk/resource/Resource.h"

#include "boost/filesystem.hpp"

#include <iostream>
#include <sstream>
#include <vector>

namespace config
{
/// Return the filename of the export operator
bool ACE3P::exportOpFilename(std::string& name) const
{
  name = "ACE3P.py";
  return true;
}

bool ACE3P::postCreate(smtk::project::ProjectPtr project) const
{
  std::cout << "Enter ACE3P::postCreate() (placeholder)" << std::endl;
  return true;
}

bool ACE3P::preExport(
  smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const
{
  int paramVersion = exportOp->parameters()->definition()->version();
  if (paramVersion != 2)
  {
    std::cerr << "Unsupported parameters version " << paramVersion;
    return false;
  }

  // Initialize exportOp with project data
  this->configNERSC(exportOp->parameters());

  // Model
  auto modelResourceItem = exportOp->parameters()->findResource("model");
  if (modelResourceItem)
  {
    auto modelResource = project->findResource<smtk::model::Resource>("default");
    modelResourceItem->setValue(modelResource);
  }

  // Attributes
  auto attResourceItem = exportOp->parameters()->findResource("attributes");
  if (attResourceItem)
  {
    auto attResource = project->findResource<smtk::attribute::Resource>("default");
    attResourceItem->setValue(attResource);
  }

  // Mesh file
  auto meshFileItem = exportOp->parameters()->findFile("MeshFile");
  this->setModelFile(meshFileItem, project, "default", exportOp->log());

  // Output folder and filename
  smtk::attribute::DirectoryItemPtr outFolderItem =
    exportOp->parameters()->itemAtPathAs<smtk::attribute::DirectoryItem>("OutputFolder");
  if (outFolderItem == nullptr)
  {
    // For backward compatibiltiy
    outFolderItem =
      exportOp->parameters()->itemAtPathAs<smtk::attribute::DirectoryItem>("Analysis/OutputFolder");
  }
  if (outFolderItem == nullptr)
  {
    std::cerr << "Warning: export paramters missing OutputFolder item";
  }

  boost::filesystem::path projectDir(project->directory());
  boost::filesystem::path outFolderPath = projectDir / "export";
  outFolderItem->setValue(0, outFolderPath.string());

  // Set filename to project name plus analysis type
  smtk::attribute::StringItemPtr outPrefixItem =
    exportOp->parameters()->itemAtPathAs<smtk::attribute::StringItem>("OutputFilePrefix");
  if (outPrefixItem == nullptr)
  {
    // For backward compatibility
    outPrefixItem =
      exportOp->parameters()->itemAtPathAs<smtk::attribute::StringItem>("AnalysisOutputFilePrefix");  }
  if (outPrefixItem == nullptr)
  {
    std::cerr << "Warning: export paramters missing OutputFilePrefix item";
  }
  outPrefixItem->setValue(project->name());

  // Check for ProjectFolder item (which might be optional)
  auto folderItem = exportOp->parameters()->findDirectory("ProjectFolder");
  if (folderItem != nullptr)
  {
    folderItem->setIsEnabled(true);
    folderItem->setValue(project->directory());
  }

  return true;
}

void ACE3P::configNERSC(smtk::attribute::AttributePtr params) const
{
  // Check if NEWT session id is in the JsonProps
  auto nsIter = this->JsonProps.find("newt_sessionid");
  if (nsIter != this->JsonProps.end())
  {
    std::string newtSessionId = *nsIter;

    // Get item for session id
    std::string itemPath = "NERSCSimulation/NERSCCredentials/NEWTSessionId";
    auto item = params->itemAtPathAs<smtk::attribute::StringItem>(itemPath);
    if (item == nullptr)
    {
      std::cerr << "Unable to find NEWTSessionId item" << std::endl;
    }
    else
    {
      item->setValue(newtSessionId);
    }
  }

  // Check if Girder URL is in JsonProps
  auto guIter = this->JsonProps.find("girder_url");
  if (guIter != this->JsonProps.end())
  {
    std::string girderUrl = *guIter;

    // Get item for session id
    std::string itemPath = "NERSCSimulation/CumulusHost";
    auto item = params->itemAtPathAs<smtk::attribute::StringItem>(itemPath);
    if (item == nullptr)
    {
      std::cerr << "Unable to find \"CumulusHost\" item" << std::endl;
    }
    else
    {
      item->setValue(girderUrl);
    }
  }

}

} // namespace config
