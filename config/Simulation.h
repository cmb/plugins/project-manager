//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef project_config_Simulation_h
#define project_config_Simulation_h

#include "config/Exports.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/model/EntityTypeBits.h"

#include "nlohmann/json.hpp"

#include <string>

namespace config
{

/* ***
 * A base class for applying simulation-specific behavior to projects
 * prior to, or following, certain project events.

*/

class PROJECTCONFIG_EXPORT Simulation
{
public:
  /// Get the filename of the export operator; return true on success
  virtual bool exportOpFilename(std::string& name) const;

  /// Apply changes after project created
  virtual bool postCreate(smtk::project::ProjectPtr project) const { return true; }

  /// Apply changes after model imported into project
  virtual bool postImportModel(smtk::project::ProjectPtr project, const std::string& role) const
  {
    return true;
  }

  /// Apply changes before project export dialog is displayed
  virtual bool preExport(
    smtk::project::ProjectPtr project, smtk::operation::OperationPtr exportOp) const
  {
    return true;
  }

  /// Assign colors to model entities
  static bool assignColors(smtk::model::ResourcePtr modelResource, smtk::model::BitFlags entityType,
    const std::vector<std::string>& colors);

  /// Assign simcode-specific properties to project resources
  virtual void tagResources(smtk::project::ProjectPtr project) const { return; }

  // A json object for config-specific properties
  nlohmann::json JsonProps;

protected:
  bool setModelFile(smtk::attribute::FileItemPtr fileItem, smtk::project::ProjectPtr project,
    const std::string& modelIdentifier, smtk::io::Logger& logger) const;
};

} // namespace config

#endif
